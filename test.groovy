@Grab(group = 'org.gitlab4j', module = 'gitlab4j-api', version = '4.18.0')
import org.gitlab4j.api.*
import org.gitlab4j.api.model.*

def POM_FILENAME = "pom.xml"
def MINOR_NUMBER_INDEX = 1

if (args.length != 6) {
    throw new IllegalArgumentException("Format of use: hostUrl personalAccessCode, projectId, branch sha commitMessage")
}

def hostUrl = args[0]
def personalAccessCode = args[1]
def projectId = args[2]
def branch = args[3]
def sha = args[4]
def commitMessage = args[5]

println "${hostUrl}, ${projectId}, ${branch}, ${sha}, ${commitMessage}"

def gitLabApi = connect(hostUrl, personalAccessCode)
def pomFileOptional = gitLabApi.getRepositoryFileApi().getOptionalFile(projectId, POM_FILENAME, sha)

if (pomFileOptional.isPresent()) {
    def pomFile = pomFileOptional.get()
    def pomContent = pomFile.getDecodedContentAsString()
    def rootNode = new XmlSlurper().parseText(pomContent)
    def semanticVersion = rootNode.properties.semanticVersion.text()
    def semanticVersionParts = semanticVersion.split("\\.")
    def minorVersion = semanticVersionParts[MINOR_NUMBER_INDEX].toInteger()
    semanticVersionParts[MINOR_NUMBER_INDEX] = minorVersion + 1

    def nextSemanticVersion = String.join(".", semanticVersionParts)

    println "next: ${nextSemanticVersion}"

    def updatedPomContent = pomContent.replaceAll("<semanticVersion>${semanticVersion}</semanticVersion>",
        "<semanticVersion>${nextSemanticVersion}</semanticVersion>")

//    def tag = gitLabApi.getTagsApi().createTag(projectId, semanticVersion, sha)

//    println tag
    pomFile.encodeAndSetContent(updatedPomContent)
    gitLabApi.getRepositoryFileApi().updateFile(projectId, pomFile, branch, commitMessage)
} else {
    throw new IllegalStateException(String.format("%s file not found, cannot get semantic version", POM_FILENAME))
}

def connect(def hostUrl,
            def personalAccessToken) {

    def gitLabApi = new GitLabApi(hostUrl, personalAccessToken)
    gitLabApi.version

    return gitLabApi
}
